NAME := lem-in

CC := gcc
CFLAGS = -Wall -Wextra -Werror -O3

SRC_DIR := src/
INC_DIR := inc/
OBJ_DIR := obj/

LIBFT_DIR := libft/
LIBFT := $(LIBFT_DIR)/libft.a
LIBFT_INC := $(LIBFT_DIR)inc

LFT_FLAGS := -lft -L $(LIBFT_DIR)

SRC := \
ants_num.c \
bfs.c \
content.c \
debug.c \
flags_parsing.c \
free_stuff.c \
graph.c \
handler.c \
hash.c \
lemin_errors.c \
link_parsing.c \
link_validation.c \
path_parsing.c \
paths_list.c \
path_print.c \
room_parsing.c \
room_validation.c \
run_ants.c \
utils.c \
main.c

OBJ := $(addprefix $(OBJ_DIR), $(SRC:.c=.o))

INC_FLAGS := -I$(INC_DIR) -I$(LIBFT_INC) -I$(LIBFT_INC)/ft_printf/

all: $(NAME)

$(NAME): $(LIBFT) $(OBJ_DIR) $(OBJ)
	@printf "\033[0;33m"
	$(CC) $(OBJ) -o $(NAME) $(INC_FLAGS) $(LFT_FLAGS)
	@printf "\033[0;32m$(NAME) compiled\n\033[0m";

$(OBJ_DIR):
	@ mkdir -p $(OBJ_DIR)
$(OBJ_DIR)%.o:$(SRC_DIR)%.c
	@printf "\033[0;33m"
	$(CC) $(CFLAGS) -c $< -o $@ $(INC_FLAGS)
$(LIBFT):
	@$(MAKE) -C $(LIBFT_DIR)

clean:
	@ if [ -e $(OBJ_DIR) ]; \
	then \
		$(RM) -r $(OBJ_DIR); \
		printf "\033[0;31m$(NAME) objects removed\n\033[0m"; \
	fi;
	@ $(MAKE) clean -C $(LIBFT_DIR)

fclean: clean
	@ if [ -e $(NAME) ]; \
	then \
		$(RM) $(NAME); \
		printf "\033[0;31m$(NAME) removed\n\033[0m"; \
	fi;
	@ $(MAKE) fclean -C $(LIBFT_DIR)

re: fclean all

.PHONY: all clean fclean re
