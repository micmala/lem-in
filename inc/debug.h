#ifndef LEM_IN_DEBUG_H
#define LEM_IN_DEBUG_H

/**
 * Printing structs utilities for debug. To activate define th DEBUG macro
 */

//#define DEBUG

void	print_graph(t_graph **pGraph);
void	print_links(t_graph **pGraph, t_adjlst *links);
void	print_one_graph(t_graph *graph);
void		print_ways(t_pathlst *all_ways);

#endif //LEM_IN_DEBUG_H
