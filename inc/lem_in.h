/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 16:34:56 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/26 16:34:57 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include "libft.h"
# include "structs.h"
# include "debug.h"

/*
********************* Errors ************************
*/

# define CMD_ERR_1 "Command before ants amount!"
# define CMD_ERR_2 "There should be only one 'start' and one 'end' command!"
# define CMD_ERR_3 "The room can not be 'start' and 'end' at the same time!"
# define AMOUNT_ERR_1 "Wrong ants amount!"
# define AMOUNT_ERR_2 "Amount of ants must be strictly positive!"
# define ROOM_ERR_1 "Room name error!"
# define ROOM_ERR_2 "Duplicate room names!"
# define ROOM_ERR_3 "Room format error!"
# define ROOM_ERR_4 "Room name can not contain '-'(hyphen) to avoid ambiguity!"
# define LOC_ERR_1 "Room location error!"
# define LOC_ERR_2 "Duplicate coordinates!"
# define LINK_ERR_1 "Links error!"
# define LINK_ERR_2 "Duplicate link!"
# define DATA_ERR "Not enough data to proceed!"
# define CONNECT_ERR "Given 'start' and 'end' rooms are not connected!"
# define READ_ERR "Read error!"
# define EMPTY_LINE "Empty line!"

/*
************************ Utils ************************
*/

void		print_usage(char *input);
char		*last_line(t_line *content);
void		print_content(t_line *lines);
void		add_content(t_line **content, char *data);
void		terminate_lemin(t_lemin *handler, char *err_msg);
int			find_index(t_graph **graph, char *name);
int			count_rooms(t_roomlst *rooms);
t_lemin		*init_lemin_handler(void);
void		free_lemin_handler(t_lemin *lemin);

t_flags		lemin_flags(int argc, char **argv);
void		print_stat(int	printed, int required);
char		*choose_color(t_path *path, bool color);

/*
*********************** Validation ***********************
*/

int			get_valid_room(char *buf, t_room *room, t_lemin *handler);
t_linklst	*get_valid_link(t_lemin *handler, char *buf);
bool		find_room_name(char *name, t_roomlst *roomlsgstt);

/*
*********************** Parse Map ************************
*/

void		get_ants_amount(t_lemin *handler);
void		get_links(t_lemin *handler);
void		get_rooms(t_lemin *handler);
void		comment(char **buf, t_lemin *handler);
void		command(t_lemin *handler, char **buf, t_room *room);

/*
*********************** Freeing Memory ************************
*/

void		free_rooms(t_roomlst *rooms);
void		free_links(t_linklst *links);
void		free_graph(t_graph **graph);
void		free_content(t_line *content);
void		free_pathlst(t_pathlst *plst);

/*
*********************** Graph ************************
*/

t_graph		**make_graph(t_linklst *links, t_roomlst *rooms);

/*
*********************** Paths ************************
*/

t_path		*parse_path(t_graph **graph, int *path_len);
bool		to_pathlst(t_pathlst **lst, t_graph **graph, int ants);
void		run_ants(t_lemin *hanlder, t_flags flags);
void		find_paths(t_graph **graph, t_lemin *handler);

#endif
