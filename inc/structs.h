/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 16:30:15 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/26 16:30:16 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H

# define STRUCTS_H
# include "lem_in.h"

typedef struct	s_point
{
	int	x;
	int	y;
}				t_point;

typedef struct	s_line
{
	char			*line;
	struct s_line	*next;
}				t_line;

typedef struct	s_room
{
	char	*name;
	bool	is_start;
	bool	is_end;
	t_point	location;
}				t_room;

typedef struct	s_roomlst
{
	t_room				*room;
	struct s_roomlst	*next;
}				t_roomlst;

typedef	struct	s_linklst
{
	char				*from;
	char				*to;
	struct s_linklst	*next;
}				t_linklst;

typedef struct	s_adjlst
{
	int				dest;
	struct s_adjlst	*next;
}				t_adjlst;

typedef struct	s_graph
{
	t_room		*room;
	int			dist;
	int			pred;
	int			index;
	bool		enqueued;
	bool		visited;
	bool		path;
	t_adjlst	*links;
}				t_graph;

typedef struct	s_path
{
	char			*room;
	int				ant_number;
	struct s_path	*head;
	struct s_path	*next;
}				t_path;

typedef struct	s_pathlst
{
	t_path				*path;
	int					length;
	int					cmp_prev;
	struct s_pathlst	*next;
}				t_pathlst;

typedef struct	s_lemin
{
	int			ants;
	bool		start;
	bool		end;
	t_line		*content;
	t_roomlst	*rooms;
	t_linklst	*links;
	t_pathlst	*paths;
	int			ln_required;
}				t_lemin;

typedef struct	s_flags
{
	bool stat;
	bool color;
	bool print_path;
	bool quiet;
}				t_flags;

#endif
