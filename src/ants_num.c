/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants_num.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:02:01 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:02:02 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/**
 * Parse number of ants from STDIN. Validates it.
 * Append the read data to line list
 * @param line
 * @return valid number of ants
 */

void	get_ants_amount(t_lemin *handler)
{
	char	*buf;
	int		gnl;

	gnl = get_next_line(STDIN_FILENO, &buf);
	if (gnl != 1)
		terminate_lemin(handler, gnl == -1 ? READ_ERR : DATA_ERR);
	while (buf[0] == '#')
		buf[1] == '#' ? terminate_lemin(handler, CMD_ERR_1)
						: comment(&buf, handler);
	if (ft_is_numeric(buf) == false)
		terminate_lemin(handler, AMOUNT_ERR_1);
	add_content(&handler->content, buf);
	if ((handler->ants = ft_atoi(buf)) < 1)
		terminate_lemin(handler, AMOUNT_ERR_2);
	ft_strdel(&buf);
}
