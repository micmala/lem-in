/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bfs.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:17:08 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:17:10 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_graph		*start(t_graph **graph)
{
	int i;

	i = 0;
	while (graph[i]->room->is_start == false)
		i++;
	return (graph[i]);
}

static void	unmark_graph(t_graph **graph)
{
	int i;

	i = 0;
	while (graph[i])
	{
		graph[i]->enqueued = false;
		graph[i]->visited = false;
		i++;
	}
}

/**
 * Breadth-First-Search algorithm to find shortest path in graph.
 * Finds only new paths.
 * @param graph
 * @return
 */

void		enqueue_links(t_queue *queue, t_adjlst *links,
		t_graph **graph, t_graph *curr)
{
	while (links)
	{
		if (graph[links->dest]->visited == false &&
			graph[links->dest]->enqueued == false &&
			graph[links->dest]->path == false)
		{
			enqueue(queue, graph[links->dest]);
			graph[links->dest]->enqueued = true;
			graph[links->dest]->dist = curr->dist + 1;
			graph[links->dest]->pred = curr->index;
		}
		links = links->next;
	}
}

bool		bfs(t_graph **graph)
{
	t_queue		*queue;
	t_graph		*curr;

	queue = create_queue();
	enqueue(queue, start(graph));
	while (queue->size != 0)
	{
		curr = dequeue(queue);
		curr->enqueued = false;
		curr->visited = true;
		enqueue_links(queue, curr->links, graph, curr);
		if (curr->room->is_end)
		{
			del_queue(queue);
			unmark_graph(graph);
			return (true);
		}
	}
	del_queue(queue);
	return (false);
}

void		find_paths(t_graph **graph, t_lemin *handler)
{
	while (bfs(graph))
	{
		if (to_pathlst(&handler->paths, graph, handler->ants) == false)
			break ;
	}
	if (handler->paths == NULL)
	{
		free_graph(graph);
		terminate_lemin(handler, CONNECT_ERR);
	}
}
