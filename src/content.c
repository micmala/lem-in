/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   content.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:02:55 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:02:56 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/**
 * Content list utils
 */

static t_line	*new_line(char *data)
{
	t_line *new;

	new = malloc(sizeof(t_line));
	new->line = ft_strdup(data);
	new->next = NULL;
	return (new);
}

char			*last_line(t_line *content)
{
	while (content->next)
		content = content->next;
	return (ft_strdup(content->line));
}

void			add_content(t_line **content, char *data)
{
	t_line *temp;

	if (!(*content))
		*content = new_line(data);
	else
	{
		temp = *content;
		while (temp->next)
			temp = temp->next;
		temp->next = new_line(data);
	}
}

void			print_content(t_line *content)
{
	while (content && content->line)
	{
		ft_putendl(content->line);
		content = content->next;
	}
	ft_putendl("");
}
