#include "lem_in.h"

/**
 * Printing structs utilities for debug. To activate define th DEBUG macro
 */

void print_links(t_graph **pGraph, t_adjlst *links)
{
	while (links)
	{
		ft_printf("[%s]->", links->dest == INT_MAX ? NULL : pGraph[links->dest]->room->name);
		links = links->next;
	}
	ft_printf("%s\n", NULL);
}

void	print_one_graph(t_graph *graph)
{
	ft_printf("Room[%d]::[%s] ", graph->index, graph->room->name);
	if (graph->room->is_start)
		ft_printf(BGRN"start"RESET);
	if (graph->room->is_end)
		ft_printf(BRED"end"RESET);
	ft_printf("\n");
	ft_printf("Visited: %s\n", graph->visited ? GRN"yes"RESET : BLK"no"RESET);
	ft_printf("Enqueued: %d\n", graph->enqueued);
	ft_printf("Distance: %d\n", graph->dist);
	ft_printf("Path: %d\n", graph->path);
	ft_printf("Pred: %d\n", graph->pred);
	ft_printf("Links: ");
//	print_links(pGraph, pGraph[i]->links);
	ft_printf("******************************************************\n");
}
void	print_graph(t_graph **pGraph)
{
	int i;

	i = 0;
	while (pGraph[i])
	{
		print_one_graph(pGraph[i]);
		i++;
	}
}
