/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags_parsing.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 15:33:32 by mmalanch          #+#    #+#             */
/*   Updated: 2019/03/13 15:33:33 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			print_usage(char *input)
{
	if (input)
		ft_dprintf(STDERR_FILENO, "lem-in: illegal option: %s\n", input);
	ft_perror("Usage: ./lem-in [-sqpch] < ant_farm_map.txt");
	ft_dprintf(STDERR_FILENO, "\n-h\t--help\t\tPrint help\n");
	ft_dprintf(STDERR_FILENO,
			"-s\t--stat\t\tPrint statistic (number of lines in ant moving)\n");
	ft_dprintf(STDERR_FILENO,
			"-q\t--quiet\t\tQuiet output. Do not print out ant map\n");
	ft_dprintf(STDERR_FILENO, "-p\t--path\t\tPrints out found paths\n");
	ft_dprintf(STDERR_FILENO,
			"-c\t--color\t\tMarks 'start' and 'end' rooms with colors\n");
	exit(EXIT_FAILURE);
}

static t_flags	init_lemin_flags(void)
{
	t_flags flags;

	flags.print_path = false;
	flags.color = false;
	flags.stat = false;
	flags.quiet = false;
	return (flags);
}

static void		read_arg(char *arg, t_flags *flags)
{
	if (ft_strequ(arg, "-s") || ft_strequ(arg, "--stat"))
		flags->stat = true;
	else if (ft_strequ(arg, "-p") || ft_strequ(arg, "-paths"))
		flags->print_path = true;
	else if (ft_strequ(arg, "-h") || ft_strequ(arg, "--help"))
		print_usage(NULL);
	else if (ft_strequ(arg, "-c") || ft_strequ(arg, "--color"))
		flags->color = true;
	else if (ft_strequ(arg, "-q") || ft_strequ(arg, "--quiet"))
		flags->quiet = true;
	else
		print_usage(arg);
}

t_flags			lemin_flags(int argc, char **argv)
{
	t_flags	flags;
	int		i;

	i = 1;
	flags = init_lemin_flags();
	while (i < argc)
	{
		read_arg(argv[i], &flags);
		i++;
	}
	return (flags);
}
