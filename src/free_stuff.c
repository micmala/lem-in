/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_stuff.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:19:18 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:19:19 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	free_rooms(t_roomlst *rooms)
{
	t_roomlst *del;

	while (rooms)
	{
		del = rooms;
		rooms = rooms->next;
		free(del->room->name);
		free(del->room);
		free(del);
	}
}

void	free_links(t_linklst *links)
{
	t_linklst *del;

	while (links)
	{
		del = links;
		links = links->next;
		ft_strdel(&del->from);
		ft_strdel(&del->to);
		free(del);
	}
}

void	free_content(t_line *content)
{
	t_line *temp;

	while (content)
	{
		temp = content;
		content = content->next;
		free(temp->line);
		free(temp);
	}
}

void	free_path(t_path *p)
{
	t_path *del;

	while (p)
	{
		del = p;
		p = p->next;
		free(del);
	}
}

void	free_pathlst(t_pathlst *plst)
{
	t_pathlst	*del_plst;

	while (plst)
	{
		del_plst = plst;
		plst = plst->next;
		free_path(del_plst->path);
		free(del_plst);
	}
}
