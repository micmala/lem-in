/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graph.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:20:42 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:20:43 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_graph	**init_graph(t_roomlst *rooms)
{
	int		rooms_count;
	t_graph	**graph_arr;
	int		i;

	rooms_count = count_rooms(rooms);
	graph_arr = malloc(sizeof(t_graph) * rooms_count + 1);
	i = 0;
	while (i < rooms_count)
	{
		graph_arr[i] = ft_memalloc(sizeof(t_graph));
		graph_arr[i]->room = rooms->room;
		graph_arr[i]->dist = graph_arr[i]->room->is_start ? 0 : INT_MAX;
		graph_arr[i]->index = i;
		graph_arr[i]->enqueued = false;
		graph_arr[i]->visited = false;
		graph_arr[i]->pred = -1;
		graph_arr[i]->links = NULL;
		rooms = rooms->next;
		i++;
	}
	graph_arr[i] = NULL;
	return (graph_arr);
}

void	free_adjlst(t_adjlst *lst)
{
	t_adjlst *temp;

	while (lst)
	{
		temp = lst;
		lst = lst->next;
		free(temp);
	}
}

void	free_graph(t_graph **graph)
{
	int i;

	i = 0;
	while (graph[i])
	{
		free_adjlst(graph[i]->links);
		free(graph[i]);
		i++;
	}
	free(graph);
}

void	append_adjlst(t_adjlst **adjlst, int dest)
{
	t_adjlst *new;

	new = malloc(sizeof(t_adjlst));
	new->dest = dest;
	new->next = *adjlst;
	*adjlst = new;
}

/**
 * Create graph from rooms and links lists.
 * Makes array of vertexes with adjacency lists with links
 * @param rooms:	room list
 * @param links:	link list
 * @return:			returns graph struct
 */

t_graph	**make_graph(t_linklst *links, t_roomlst *rooms)
{
	t_graph	**graph;
	int		from;
	int		to;

	graph = init_graph(rooms);
	while (links)
	{
		from = find_index(graph, links->from);
		to = find_index(graph, links->to);
		append_adjlst(&graph[from]->links, to);
		append_adjlst(&graph[to]->links, from);
		links = links->next;
	}
	return (graph);
}
