/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 19:13:46 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/26 19:13:47 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_lemin	*init_lemin_handler(void)
{
	t_lemin *handler;

	handler = ft_memalloc(sizeof(t_lemin));
	handler->ants = 0;
	handler->content = NULL;
	handler->links = NULL;
	handler->rooms = NULL;
	handler->paths = NULL;
	handler->end = false;
	handler->start = false;
	handler->ln_required = -1;
	return (handler);
}

void	free_lemin_handler(t_lemin *lemin)
{
	free_links(lemin->links);
	free_rooms(lemin->rooms);
	free_content(lemin->content);
	free_pathlst(lemin->paths);
	free(lemin);
}
