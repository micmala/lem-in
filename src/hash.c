/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:03:47 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:03:49 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#define START "##start"
#define END "##end"

/**
 * Parses comment from buffered line. Reads next line to buffer pointer
 * @param content
 * @param buf
 */

#define GEN_LN "#Here is the number of lines required:"
#define GEN_LN_LEN 38

void	comment(char **buf, t_lemin *handler)
{
	add_content(&handler->content, *buf);
	if (handler->ln_required == -1 && ft_strnequ(*buf, GEN_LN, GEN_LN_LEN))
		handler->ln_required = ft_atoi(*buf + GEN_LN_LEN);
	ft_strdel(buf);
	if (get_next_line(STDIN_FILENO, buf) == -1)
		terminate_lemin(handler, READ_ERR);
}

/**
 * Parse command (start | end) from buffer.
 * Sets room (is_start | is_end) property
 * If do not know a command, treats it like a comment
 * @param content pointer
 * @param buf pointer
 * @param room pointer
 */

void	command(t_lemin *handler, char **buf, t_room *room)
{
	if (ft_strequ(*buf, START))
	{
		handler->start ? terminate_lemin(handler, CMD_ERR_2)
		: (handler->start = true);
		room->is_start = true;
		ft_strdel(buf);
		if (get_next_line(STDIN_FILENO, buf) == -1)
			terminate_lemin(handler, READ_ERR);
		if (ft_strequ(*buf, START) || ft_strequ(*buf, END))
			terminate_lemin(handler, CMD_ERR_3);
	}
	else if (ft_strequ(*buf, END))
	{
		handler->end ? terminate_lemin(handler, CMD_ERR_2)
		: (handler->end = true);
		room->is_end = true;
		ft_strdel(buf);
		if (get_next_line(STDIN_FILENO, buf) == -1)
			terminate_lemin(handler, READ_ERR);
		if (ft_strequ(*buf, START) || ft_strequ(*buf, END))
			terminate_lemin(handler, CMD_ERR_3);
	}
	if (*buf && *buf[0] == '#')
		comment(buf, handler);
}
