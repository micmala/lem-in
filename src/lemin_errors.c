/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin_errors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:04:31 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:04:32 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include "lem_in.h"

void	terminate_lemin(t_lemin *handler, char *err_msg)
{
	free_lemin_handler(handler);
	ft_putstr_fd(RED"Lem-in:[ERROR] ", STDERR_FILENO);
	if (errno == 0)
		ft_perror(err_msg);
	else
		perror(err_msg);
	exit(EXIT_FAILURE);
}
