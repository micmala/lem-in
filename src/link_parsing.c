/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:04:55 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:04:56 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void	append_to_linklst(t_linklst **lst, t_linklst *link)
{
	if (link && lst)
	{
		link->next = *lst;
		*lst = link;
	}
}

/**
 * Read links and validates them until the EOF. Throws error on non-valid.
 * @param content
 * @param roomlst
 * @return
 */

void		get_links(t_lemin *handler)
{
	char *buf;

	buf = last_line(handler->content);
	while (buf)
	{
		if (ft_strequ(buf, "##start") || ft_strequ(buf, "##end"))
			terminate_lemin(handler, CMD_ERR_2);
		if (buf[0] != '#')
			append_to_linklst(&handler->links, get_valid_link(handler, buf));
		ft_strdel(&buf);
		if (get_next_line(STDIN_FILENO, &buf) == -1)
			terminate_lemin(handler, READ_ERR);
		if (ft_strequ(buf, ""))
			terminate_lemin(handler, EMPTY_LINE);
		add_content(&handler->content, buf);
	}
}
