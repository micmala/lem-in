/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link_validation.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:05:44 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:05:46 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/**
 * Checks the presence of current link in the links list
 * @param lst
 * @param link
 * @return
 */

static bool	find_link(t_linklst *lst, t_linklst *link)
{
	while (lst)
	{
		if ((ft_strequ(lst->from, link->from) && ft_strequ(lst->to, link->to))
		|| (ft_strequ(lst->to, link->from) && ft_strequ(lst->from, link->to)))
			return (true);
		lst = lst->next;
	}
	return (false);
}

/**
 * Validates link from buffered line.
 * @param linklst
 * @param roomlst
 * @param buf
 * @return: valid link. Throws error on non-valid.
 */

t_linklst	*get_valid_link(t_lemin *handler, char *buf)
{
	t_linklst	*link;
	char		*dash;

	link = malloc(sizeof(t_linklst));
	link->next = NULL;
	dash = ft_strchr(buf, '-');
	if (dash == NULL)
		terminate_lemin(handler, LINK_ERR_1);
	link->from = ft_strndup(buf, dash - buf);
	if (find_room_name(link->from, handler->rooms) == false)
		terminate_lemin(handler, LINK_ERR_1);
	buf = ft_strchr(buf, '-') + 1;
	link->to = ft_strdup(buf);
	if (find_room_name(link->to, handler->rooms) == false)
		terminate_lemin(handler, LINK_ERR_1);
	if (find_link(handler->links, link) == true)
		terminate_lemin(handler, LINK_ERR_2);
	return (link);
}
