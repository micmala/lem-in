/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:21:34 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:21:35 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

//#define DEBUG

/**
 * General parser of ant_farm. Contains all module functions for parsing
 * @param content:	pointer to ant_farm content to read and print out in the end
 * @param ants:		pointer to number of ants to be parsed to
 * @param rooms:	pointer to room(vertexes) list
 * @param links:	pointer to links(edges) list
 */

/*
 * TODO improve algorithm to pass superposition
 */

void	parse_ant_farm(t_lemin *handler)
{
	get_ants_amount(handler);
	get_rooms(handler);
	get_links(handler);
}

void	lem_in(t_flags flags)
{
	t_graph		**graph;
	t_lemin		*handler;

	handler = init_lemin_handler();
	parse_ant_farm(handler);
	graph = make_graph(handler->links, handler->rooms);
	find_paths(graph, handler);
	if (flags.quiet == false)
		print_content(handler->content);
	if (flags.print_path)
		print_ways(handler->paths);
	run_ants(handler, flags);
#ifdef DEBUG
	print_graph(graph);
#endif
	free_graph(graph);
	free_lemin_handler(handler);
}

int		main(int argc, char **argv)
{
	//redirecting file stream to STDIO
	freopen("./ant_farm", "r", stdin);
	lem_in(lemin_flags(argc, argv));
	return (0);
}
