/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:07:05 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:07:07 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		pathlst_push_front(t_path **way_ptr, char *room_name)
{
	t_path *new_way;

	new_way = malloc(sizeof(t_path));
	new_way->room = room_name;
	new_way->ant_number = -1;
	new_way->next = *way_ptr;
	*way_ptr = new_way;
}

static t_graph	*find_end(t_graph **graph)
{
	int i;

	i = 0;
	while (graph[i]->room->is_end == false)
		i++;
	return (graph[i]);
}

t_path			*parse_path(t_graph **graph, int *path_len)
{
	t_path	*way;
	t_graph	*end;
	int		curr;

	end = find_end(graph);
	way = NULL;
	pathlst_push_front(&way, end->room->name);
	curr = end->pred;
	while (curr != -1)
	{
		if (!graph[curr]->path)
		{
			pathlst_push_front(&way, graph[curr]->room->name);
			if (graph[curr]->room->is_start == false)
				graph[curr]->path = true;
		}
		curr = graph[curr]->pred;
		(*path_len)++;
	}
	way->next->head = way->next;
	return (way);
}
