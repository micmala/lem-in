/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_print.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 15:46:04 by mmalanch          #+#    #+#             */
/*   Updated: 2019/03/13 15:46:05 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void	print_way(t_path *path)
{
	while (path)
	{
		ft_printf(GRN"[%s]"RESET, path->room);
		if (path->next)
			ft_printf("->");
		path = path->next;
	}
	ft_printf(RESET"\n");
}

void		print_ways(t_pathlst *all_ways)
{
	ft_printf("*********************************************************\n\n");
	while (all_ways)
	{
		ft_printf("Path length: %d\n", all_ways->length);
		print_way(all_ways->path);
		all_ways = all_ways->next;
	}
	ft_printf("\n*********************************************************\n");
}
