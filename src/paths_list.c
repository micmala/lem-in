/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paths_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:08:27 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:08:28 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_pathlst	*new_waylst(t_graph **graph)
{
	t_pathlst *new;

	new = malloc(sizeof(t_pathlst));
	new->length = 0;
	new->cmp_prev = 0;
	new->path = parse_path(graph, &new->length);
	new->next = NULL;
	return (new);
}

static bool			add_to_pathlst(
		t_pathlst **lst, t_pathlst *new_way, int ants)
{
	t_pathlst *temp;

	if (*lst == NULL)
		*lst = new_way;
	else
	{
		temp = *lst;
		while (temp->next)
		{
			new_way->cmp_prev += new_way->length - temp->length;
			temp = temp->next;
		}
		new_way->cmp_prev += new_way->length - temp->length;
		if (ants > new_way->cmp_prev)
			temp->next = new_way;
		else
			return (false);
	}
	return (true);
}

bool				to_pathlst(t_pathlst **lst, t_graph **graph, int ants)
{
	t_pathlst *new_way;

	new_way = new_waylst(graph);
	if (*lst && (*lst)->length == 1 && new_way->length == 1)
	{
		free_pathlst(new_way);
		return (false);
	}
	if (add_to_pathlst(lst, new_way, ants) == false)
	{
		free_pathlst(new_way);
		return (false);
	}
	return (true);
}
