/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:09:20 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:09:22 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_room	*init_room(void)
{
	t_room *new;

	new = malloc(sizeof(t_room));
	new->name = NULL;
	new->is_start = false;
	new->is_end = false;
	new->location.x = -1;
	new->location.y = -1;
	return (new);
}

static void		append_to_roomlist(t_roomlst **roomlst, t_room *room)
{
	t_roomlst *temp;

	if (room)
	{
		temp = malloc(sizeof(t_roomlst));
		temp->room = room;
		temp->next = *roomlst;
		*roomlst = temp;
	}
}

/**
 * Read rooms from buffer and validate them until finds a link.
 * @param content
 * @return:	returns room list parsed or NULL on error
 */

void			get_rooms(t_lemin *handler)
{
	t_room		*curr;
	char		*buf;
	int			count;

	count = 0;
	while (get_next_line(STDIN_FILENO, &buf) == 1)
	{
		curr = init_room();
		while (buf && buf[0] == '#')
			buf[1] == '#' ? command(handler, &buf, curr)
								: comment(&buf, handler);
		if (ft_strequ(buf, ""))
			terminate_lemin(handler, EMPTY_LINE);
		add_content(&handler->content, buf);
		if (get_valid_room(buf, curr, handler) == -1)
			return ;
		append_to_roomlist(&handler->rooms, curr);
		ft_strdel(&buf);
		count++;
	}
	terminate_lemin(handler, DATA_ERR);
}
