/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room_validation.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:13:35 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:13:37 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#define LOC_SYM "0123456789-+"
#define IS_INTEGER(x) !((x) < INT_MIN || (x) > INT_MAX)

/**
 *	functions to validate room (name, location, command)
 */

/**
 * checks whether there is only 1 start command and 1 end command
 * @param roomlst
 * @return
 */

static bool		valid_commands(t_roomlst *roomlst)
{
	int	start;
	int	end;

	start = 0;
	end = 0;
	while (roomlst)
	{
		start += roomlst->room->is_start;
		end += roomlst->room->is_end;
		roomlst = roomlst->next;
	}
	return (start == 1 && end == 1);
}

/**
 * Extracts room name from buffered line. Throws error on non-valid.
 * @param buf:		current buffered line
 * @param roomlst:	whole room list
 * @return:
 */

static char		*get_room_name(char *buf, t_lemin *handler)
{
	char *name;
	char *space;

	space = ft_strchr(buf, ' ');
	if (space == NULL)
		terminate_lemin(handler, ROOM_ERR_3);
	name = ft_strndup(buf, space - buf);
	if (ft_strchr(name, '-'))
		terminate_lemin(handler, ROOM_ERR_4);
	if (name == NULL || ft_strequ(name, "") || name[0] == 'L')
		terminate_lemin(handler, ROOM_ERR_1);
	if (find_room_name(name, handler->rooms) == true)
		terminate_lemin(handler, ROOM_ERR_2);
	return (name);
}

static bool		check_location(t_point loc, t_roomlst *rooms)
{
	while (rooms)
	{
		if (loc.x == rooms->room->location.x
			&& loc.y == rooms->room->location.y)
			return (false);
		rooms = rooms->next;
	}
	return (true);
}

/**
 * Extracts room location {x, y} point from buffered line.
 * Throws error on non-valid.
 * @param buf
 * @param roomlst
 * @return
 */

static t_point	get_location(char *buf, t_lemin *handler)
{
	t_point	loc;
	long	temp_x;
	long	temp_y;

	buf = ft_strchr(buf, ' ') + 1;
	if (buf == NULL || ft_strequ(buf, "") || !ft_strchr(LOC_SYM, buf[0]))
		terminate_lemin(handler, LOC_ERR_1);
	temp_x = ft_strtol(buf, &buf, 10);
	buf++;
	if (buf == NULL || ft_strequ(buf, "") || !ft_strchr(LOC_SYM, buf[0]))
		terminate_lemin(handler, LOC_ERR_1);
	temp_y = ft_strtol(buf, &buf, 10);
	if (!IS_INTEGER(temp_x) || !IS_INTEGER(temp_y))
		terminate_lemin(handler, LOC_ERR_1);
	loc.x = (int)temp_x;
	loc.y = (int)temp_y;
	if (check_location(loc, handler->rooms) == false)
		terminate_lemin(handler, LOC_ERR_2);
	ft_strequ(buf, "") ? 0 : terminate_lemin(handler, ROOM_ERR_1);
	return (loc);
}

/**
 * Parse room name and location. Throws error on non-valid buffer.
 * @param buf
 * @param room
 * @param roomlst
 * @return:	(0) in case of success room parsing; (-1) if link was parsed
 */

int				get_valid_room(char *buf, t_room *room, t_lemin *handler)
{
	if (ft_strchr(buf, '-') && !ft_strchr(buf, ' '))
	{
		if (!valid_commands(handler->rooms))
			terminate_lemin(handler, CMD_ERR_2);
		free(room);
		ft_strdel(&buf);
		return (-1);
	}
	room->name = get_room_name(buf, handler);
	room->location = get_location(buf, handler);
	return (0);
}
