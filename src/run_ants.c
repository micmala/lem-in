/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_ants.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:14:48 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:14:49 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
 * Algorithm is :
 *
 * There is a list (t_pathlst) of all paths
 * 1. While there are ants at start, set each ant to particular path
 *  (just write down the ant_number to the path);
 * 	If all paths starts are occupied all ants make a move
 * 2. When all ants chose their paths, make all ants move until no move was made
 *
 */

static void	move_one_path(t_path *path, int *moved, bool color_out)
{
	int curr;
	int prev;

	prev = -1;
	while (path && path->ant_number == -1)
		path = path->next;
	while (path)
	{
		curr = path->ant_number;
		path->ant_number = prev;
		prev = curr;
		if (curr == -1)
			break ;
		if (!path->next)
		{
			path->ant_number = -1;
			break ;
		}
		ft_printf("%sL%d-%s "RESET,
				choose_color(path, color_out), curr, path->next->room);
		(*moved)++;
		path = path->next;
	}
}

static bool	move_ants(t_pathlst *all_ways, int *count, bool color_out)
{
	int			ants_moved;
	t_pathlst	*curr_way;

	curr_way = all_ways;
	ants_moved = 0;
	while (curr_way)
	{
		move_one_path(curr_way->path, &ants_moved, color_out);
		curr_way = curr_way->next;
	}
	if (ants_moved)
	{
		ft_putendl("");
		(*count)++;
	}
	return (ants_moved != 0);
}

static bool	choose_path(t_pathlst *all_ways, int *ant, int ants_at_start)
{
	t_pathlst *curr_way;

	curr_way = all_ways;
	while (curr_way)
	{
		if (curr_way->path->ant_number == -1 &&
		ants_at_start > curr_way->cmp_prev)
		{
			curr_way->path->ant_number = *ant;
			(*ant)++;
			return (true);
		}
		curr_way = curr_way->next;
	}
	return (false);
}

static void	set_ants_on_paths(
		t_pathlst *lst, int initial_ants, int *count, bool color_out)
{
	int ant;
	int at_start;

	ant = 1;
	while (ant <= initial_ants)
	{
		at_start = initial_ants - ant + 1;
		if (choose_path(lst, &ant, at_start) == false)
			move_ants(lst, count, color_out);
	}
}

void		run_ants(t_lemin *hanlder, t_flags flags)
{
	int ln_printed;

	ln_printed = 0;
	set_ants_on_paths(hanlder->paths, hanlder->ants, &ln_printed, flags.color);
	while (move_ants(hanlder->paths, &ln_printed, flags.color))
		;
	if (flags.stat)
		print_stat(ln_printed, hanlder->ln_required);
}
