/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:16:54 by mmalanch          #+#    #+#             */
/*   Updated: 2019/02/23 18:16:56 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/**
 * Checks the presence of current room in the rooms list
 * @param lst
 * @param link
 * @return
 */

char	*choose_color(t_path *path, bool color)
{
	if (color)
	{
		if (path->next == path->next->head)
			return (GRN);
		else if (path->next->next == NULL)
			return (RED);
	}
	return (RESET);
}

void	print_stat(int printed, int required)
{
	ft_printf("\n");
	if (required != -1)
	{
		if (required == printed)
			ft_printf(YEL);
		else
			ft_printf(required > printed ? GRN : RED);
		ft_printf("Required:\t%d\n", required);
	}
	ft_printf("Printed:\t%d\n", printed);
	if (required != -1)
		ft_printf("Diff:\t\t%d\n", printed - required);
	ft_printf(RESET);
}

bool	find_room_name(char *name, t_roomlst *roomlst)
{
	while (roomlst)
	{
		if (ft_strequ(roomlst->room->name, name))
			return (true);
		roomlst = roomlst->next;
	}
	return (false);
}

int		count_rooms(t_roomlst *rooms)
{
	int	count;

	count = 0;
	while (rooms)
	{
		count++;
		rooms = rooms->next;
	}
	return (count);
}

int		find_index(t_graph **graph, char *name)
{
	int index;

	index = 0;
	while (!ft_strequ(graph[index]->room->name, name))
		index++;
	return (index);
}
